import datetime
import pickle
import os

import matplotlib.pyplot as plt
import torch


from encoder_decoder import EncoderDecoderConvLSTM
from wavefront_data import WavefrontData
from training_settings import opt
from moving_wavefront import MovingWFLightning
import gc

model = None
gc.collect()
torch.cuda.empty_cache()

train_data = WavefrontData(opt.train_path)
val_data = WavefrontData(opt.val_path)
test_data = WavefrontData(opt.test_path)

conv_lstm_model = EncoderDecoderConvLSTM(nf=opt.n_hidden_dim, in_chan=1).to("cuda")
model = MovingWFLightning(model=conv_lstm_model).to("cuda")

if __name__ == "__main__":

    #ОБУЧЕНИЕ
    history = []
    thistory = []
    lr_history = []
    last_epoch = 0

    lr = opt.lr
    optimizer =  torch.optim.Adam(model.parameters(), lr=lr)

    for epoch in range(last_epoch, opt.epochs):
        j = 0
        losses = 0
        tlosses = 0
        for i,batch in enumerate(train_data):
            optimizer.zero_grad()
            j+=1
            loss = model.training_step(batch.to("cuda"), i)
            losses+=loss["loss"]
            optimizer.step()
        for k,tbatch in enumerate(test_data):
            test_loss = model.test_step(tbatch.to("cuda"), k)
            tlosses+=test_loss["loss"]
            test_loss["loss"].backward()
        thistory.append(tlosses/len(test_data))
        history.append(losses/j)
        last_epoch = epoch
        lr_history.append(lr)
        print("Epoch %d loss = %f, test loss = %f (count of batch %d)"%(epoch, losses/j, tlosses/5, j))
        if losses/j < 0.0001:
            break

    #ВЫГРУЗКА

    check_file_hist = os.path.exists('history')
    check_file_conf = os.path.exists('configs')
    check_file_model = os.path.exists('model')

    if not check_file_conf:
        os.mkdir("./configs")
    if not check_file_hist:
        os.mkdir("./history")
    if not check_file_model:
        os.mkdir("./model")

    model_name ="mse_conv_lstm_%d_%d_%d_%d"%(datetime.datetime.now().day,
                                                      datetime.datetime.now().month,
                                                      datetime.datetime.now().hour,
                                                      datetime.datetime.now().minute)
    plt.figure(figsize=(10, 6))
    plt.plot(history, label="train")
    plt.plot(thistory, label="test")
    plt.xlabel("epoch")
    plt.ylabel("losses")
    plt.title("MSE ")
    plt.savefig("history/%s.png"%model_name)
    plt.legend()
    plt.grid()

    torch.save(model.state_dict(), "model/%s.torch"%model_name)

    with open('configs/%s.pickle'%model_name, 'wb') as f:
        pickle.dump(opt.asdict(),f)
