import soapy
import numpy as np
import cv2
import torch
import yaml

CONF_FILE = "/conf/def.yaml"

def random_config():
    with open(CONF_FILE, 'r') as stream:
        data_loaded = yaml.safe_load(stream)
    data_loaded['Atmosphere']['windDirs'] = np.random.randint(0,180,4).tolist()
    new_file_name =  '/conf/def.yaml'
    with open(new_file_name, 'w') as f:
        yaml.dump(data_loaded.copy(), f, default_flow_style=False)
    return new_file_name

def generate(count_data = 100, period = 20, random_conf = False):
    count = 0
    X = []
    Y_sumphase = []
    Y_coefs = []
    while count!= count_data:
        if random_conf:
            conf_name = random_config()
        else:
            conf_name = CONF_FILE
        sim = soapy.Sim(conf_name)
        config = soapy.confParse.loadSoapyConfig(conf_name)
        sim.aoinit()
        atmosphere = soapy.atmosphere.atmos(config)
        sim.makeIMat()
        for i in range(1, period+1):
            sim.makeIMat()
            sim.loopFrame()
            if i == 1: continue 
            x = None
            y_sum = None
            y_coeffs = None
            y_sum = sim.dms[1].dm_shape
            y_sum = cv2.resize(y_sum, (64, 64)).reshape(1,64,64)
            x = sim.sciImgs[0].copy().reshape( 1, 64, 64)
            x = (x - np.min(x))/(np.max(x) - np.min(x))
            img = sim.sciImgs[1].copy().reshape(1, 64, 64)
            img = (img - np.min(img))/(np.max(img) - np.min(img))
            x = np.append(x, img, axis=0)
            y_coeffs = sim.dmCommands[sim.dmAct1[1]:sim.dmAct1[1] + sim.dms[1].n_valid_actuators]
            X.append(x)
            Y_sumphase.append(y_sum)
            Y_coefs.append(y_coeffs)
            print("\n %d / %d COUN OF DATA \n"%(count, count_data))
            if count== count_data:
                break
            count+=1
    return X, Y_sumphase, Y_coefs

def preprocessing(X, Y_sum, Y_coefs):
    X = np.asarray(X)
    Y_sum = np.asarray(Y_sum)
    Y_coefs = np.asarray(Y_coefs)
    Y_sum = (Y_sum - np.min(Y_sum))/(np.max(Y_sum) - np.min(Y_sum))
    X_train = torch.from_numpy(X).float()
    y_train_sum = torch.from_numpy(Y_sum).float()
    y_train_coefs = torch.from_numpy(Y_coefs).float()
    return X_train, y_train_sum, y_train_coefs

def future_prediction_batch(count_data = 100, period = 20):
    X, Y_sum, Y_coef = generate(count_data+1, period)
    X = X[:-1]
    Y_sum = Y_sum[1:]
    Y_coef = Y_coef[1:]    
    return preprocessing(X, Y_sum, Y_coef)

def present_prediction_batch(count_data = 100, period = 20):
    X, Y_sum, Y_coef = generate(count_data, period)
    return preprocessing(X, Y_sum, Y_coef)


if __name__ == "__main__":
    future_prediction_batch()